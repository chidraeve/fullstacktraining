name := "fullstacktraining"
 
version := "1.0" 
      
lazy val `fullstacktraining` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.2"

libraryDependencies ++= Seq( jdbc , ehcache , ws ,  guice, "com.typesafe.play" %% "anorm" % "2.5.3" )

libraryDependencies ++= Seq("org.specs2" %% "specs2-core" % "4.3.4" % "test")

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.41"

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

      