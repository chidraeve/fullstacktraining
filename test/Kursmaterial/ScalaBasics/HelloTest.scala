package Kursmaterial.ScalaBasics


import org.specs2.mutable._

class HelloTest extends Specification {

  "The 'Hallo Welt' string" should {
    "contain 11 characters" in {
      "Hallo Welt" must have size(10)
    }
    "start with 'Hallo'" in {
      "Hallo Welt" must startWith("Hallo")
    }
    "end with 'Welt'" in {
      "Hallo Welt" must endWith("Welt")
    }
  }

}
