package controllers

import javax.inject._
import play.api.mvc._


@Singleton
class DocsController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {


  def index = Action {
    Ok(views.html.docs.index("Dokumentation"))
  }

}
